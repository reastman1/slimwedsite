<?php
namespace ViewHelper;

class pageViews {
    public function __invoke($page){
        return __DIR__ . "/../templates/" . $page . ".phtml";
    }
}
